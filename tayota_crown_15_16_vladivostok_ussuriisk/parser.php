<?php  declare(strict_types=1);

use DromParser\Result\ResultInterface;
use DromParser\Utils\ProxyKeeper;
use DromParser\Utils\ResourceReader;
use DromParser\WebParser;
use DromParser\WebParser\Filter\FilterAuto;
use DromParser\WebParser\Filter\Types\Auto;
use DromParser\WebParser\Filter\Types\Brand;
use DromParser\WebParser\Filter\Types\City;
use DromParser\WebParser\Filter\Types\Damage;
use DromParser\WebParser\Filter\Types\Documents;
use DromParser\WebParser\Filter\Types\Model;
use League\Csv\CannotInsertRecord;
use League\Csv\Writer;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Level;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use ZipStream\ZipStream;

require_once 'vendor/autoload.php';

const PATH_TO_LOG_FILE = __DIR__ . '/logs/parser.log';
const PATH_TO_PROXY_FILE = __DIR__ . '/proxy.json';
const PATH_TO_ZIP_FILE = __DIR__ . '/Result_Crown.zip';
const CSV_FILE_NAME = 'Data.csv';

$logger = getLogger();

try {
    $proxyKeeper = ProxyKeeper::createFromFile(PATH_TO_PROXY_FILE);
    $webParser = new WebParser($proxyKeeper, $logger);

    $filterAuto = (new FilterAuto())
        // выбираем Toyota Crown 15 и 16 поколения
        ->setAutos(
            new Auto(Brand::TOYOTA, Model::CROWN, 15),
            new Auto(Brand::TOYOTA, Model::CROWN, 16),
        )
        // в городах Владивосток и Уссурийск
        ->setCities(City::VLADIVOSTOK, City::USSURIISK)
        // не требующих ремонта
        ->setDamage(Damage::UNDAMAGED)
        // документы в порядке
        ->setDocuments(Documents::OK)
        // не проданы
        ->setUnsold();

    $resultAutos = $webParser->parseAuto($filterAuto);
    $csvContent = getCsvContent($resultAutos);

    if (!empty($csvContent)) {
        $zipHandle = fopen(PATH_TO_ZIP_FILE, 'w');
        $zipStream = new ZipStream(outputStream: $zipHandle);

        $zipStream->addFile(CSV_FILE_NAME, $csvContent);

        downloadPhotos(
            $resultAutos,
            $zipStream,
            new ResourceReader($proxyKeeper, $logger)
        );

        $zipStream->finish();
        fclose($zipHandle);
    }
} catch (Throwable $t) {
    $logger->error("Parsing error: {$t->getMessage()}");
}

function getLogger(): LoggerInterface
{
    $logger = new Logger('AUTO');
    $lineFormatter = new LineFormatter(
        dateFormat: 'Y-m-d H:i:s',
        ignoreEmptyContextAndExtra: true
    );

    $consoleHandler = new StreamHandler('php://stdout', Level::Debug);
    $consoleHandler->setFormatter($lineFormatter);
    $logger->pushHandler($consoleHandler);

    $fileHandler = new StreamHandler(PATH_TO_LOG_FILE, Level::Debug);
    $fileHandler->setFormatter($lineFormatter);
    $logger->pushHandler($fileHandler);

    return $logger;
}

/**
 * @param ResultInterface[] $resultAutos
 * @throws CannotInsertRecord
 * @throws League\Csv\Exception
 */
function getCsvContent(array $resultAutos): ?string
{
    $csvContent = [];

    foreach ($resultAutos as $resultAuto) {
        $dataAuto = $resultAuto->toArray();

        $fuel = [];
        $dealType = [];

        if (!empty($dataAuto['fuel'])) {
            $fuel[] = $dataAuto['fuel'];
        }

        if (!empty($dataAuto['isHybrid'])) {
            $fuel[] = $dataAuto['isHybrid'];
        }

        if (!empty($dataAuto['isGbo'])) {
            $fuel[] = $dataAuto['isGbo'];
        }

        if (!empty($dataAuto['dealType'])) {
            $dealType[] = $dataAuto['dealType'];
        }

        if (!empty($dataAuto['dromPrice'])) {
            $dealType[] = $dataAuto['dromPrice'];
        }

        $csvContent[] = [
            $dataAuto['dromId'] ?? 'null',
            $dataAuto['url'] ?? 'null',
            $dataAuto['brand'] ?? 'null',
            $dataAuto['model'] ?? 'null',
            $dataAuto['price'] ?? 'null',
            !empty($dealType) ? implode(', ', $dealType) : 'null',
            $dataAuto['generation'] ?? 'null',
            $dataAuto['complectation'] ?? 'null',
            $dataAuto['mileage'] ?? 'null',
            $dataAuto['mileageInRussia'] ?? 'null',
            $dataAuto['color'] ?? 'null',
            $dataAuto['frameType'] ?? 'null',
            $dataAuto['power'] ?? 'null',
            !empty($fuel) ? implode(', ', $fuel) : 'null',
            $dataAuto['volume'] ?? 'null',
        ];
    }

    if (!empty($csvContent)) {
        $csv = Writer::createFromString();
        $csv->insertAll($csvContent);

        return $csv->toString();
    }

    return null;
}

/**
 * @param ResultInterface[] $resultAutos
 */
function downloadPhotos(array $resultAutos, ZipStream $zipStream, ResourceReader $resourceReader): void
{
    foreach ($resultAutos as $resultAuto) {
        $dataAuto = $resultAuto->toArray();

        if (empty($dataAuto['photoUrls'])) {
            continue;
        }

        $zipStream->addDirectory((string)$dataAuto['dromId']);

        foreach ($dataAuto['photoUrls'] as $photoUrl) {
            $baseName = pathinfo($photoUrl, PATHINFO_BASENAME);
            $zipStream->addFile(
                "{$dataAuto['dromId']}/$baseName",
                $resourceReader->getContent($photoUrl)
            );
        }
    }
}
