$RUN_ARGS = $Args
$WORKDIR = "/home/parse-drom"
$DOCKER_IMAGE="parse-drom:latest"

if (!(docker images -q ${DOCKER_IMAGE} 2> $null)) {
    docker build -t ${DOCKER_IMAGE} ${PWD}
}

docker `
    run --rm `
    -v ${PWD}:${WORKDIR} `
    -w ${WORKDIR} `
    ${DOCKER_IMAGE} `
    ${RUN_ARGS}
