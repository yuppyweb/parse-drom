#!/usr/bin/env bash

RUN_ARGS=${*}
WORKDIR="/home/parse-drom"
DOCKER_IMAGE="parse-drom:latest"

if [[ "$(docker images -q ${DOCKER_IMAGE} 2> /dev/null)" == "" ]]; then
  docker build -t ${DOCKER_IMAGE} ${PWD}
fi

docker \
    run --rm \
    -v ${PWD}:${WORKDIR} \
    -w ${WORKDIR} \
    ${DOCKER_IMAGE} \
    ${RUN_ARGS}
