FROM php:8.2-cli-alpine

RUN set -xe \
    && apk update \
    && apk add bash \
    && apk add git

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && chmod +x /usr/local/bin/composer \
    && composer clear-cache

ENTRYPOINT ["./entrypoint.sh"]
